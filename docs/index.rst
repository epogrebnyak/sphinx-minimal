sphinx-minimal
==============

This is a minimal configuration for Sphinx documentation. 
There are just two files in ``/docs`` folder: ``conf.py`` and  ``index.rst``.

conf.py
-------

``conf.py`` controls documentation parameters. This a python script that
assigns values to the variables wanted by Sphinx to fill in into the
documentation template as well as names for extensions and a theme name. 
You can look ``conf.py`` at the repository, or below:

.. code:: python

   project = "Seemingly Simple Project"
   author = "I.M. Coder"
   copyright = f"2021, {author}"
   extensions = [
       "sphinx.ext.autodoc",
       "sphinx_rtd_theme",
   ]
   html_theme = "sphinx_rtd_theme"

index.rst
---------

``index.rst`` is a text file that contains the 
contents of a page you are at now.

It is written in reStructuredText, a useful, but slightly 
awkward markup language. You can setup sphinx to use markdown, 
a simpler formatting language.

You can see ``index.rst`` if you click *View page source* link
above or `here <_sources/index.rst.txt>`__.
