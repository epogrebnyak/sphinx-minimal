"""Sphinx configuration."""
# Adapted from cjolowicz/cookiecutter-hypermodern-python
# https://bit.ly/3bRSBQ1

project = "Seemingly Simple Project"
author = "I.M. Coder"
copyright = f"2021, {author}"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx_rtd_theme",
]
html_theme = "sphinx_rtd_theme"