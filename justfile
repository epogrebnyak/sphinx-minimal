S3_URL := "s3://sphinx-minimal"
site_folder := "public"

clean:
   rm -rf {{site_folder}}

build:
   sphinx-build -b html docs {{site_folder}}

show:
   start {{site_folder}}/index.html

trigger:
   git commit --allow-empty -m "trigger new build"

just-reinstall:
   curl --proto '=https' --tlsv1.2 -sSf https://just.systems/install.sh | bash -s -- --to DEST

aws-update:
   pip install -U awscli

s3-website:
   aws s3 website {{S3_URL}} --index-document index.html --error-document error.html

s3-copy:
   aws s3 cp {{site_folder}} {{S3_URL}} --recursive --acl public-read

s3-remove:
   aws s3 rm {{S3_URL}}  --recursive