[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/epogrebnyak/sphinx-minimal)
 
# Sphinx Minimal

Minimal example of Sphinx documentation setup - check out the [docs](docs) folder

Example site URLs: 

- <https://epogrebnyak.gitlab.io/sphinx-minimal>
- <http://sphinx-minimal.s3-website.eu-central-1.amazonaws.com/>


See <https://github.com/epogrebnyak/borrowed-sphinx/> for more examples.
